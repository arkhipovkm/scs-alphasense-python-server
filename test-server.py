import cherrypy
import os
import json
from subprocess import Popen, PIPE

### Copy of imports from gases_sampler.py
from scs_host.bus.i2c import I2C
from scs_host.sys.host import Host
import sys
from scs_core.data.json import JSONify
from scs_core.data.localized_datetime import LocalizedDatetime
from scs_core.sync.timed_runner import TimedRunner
from scs_core.sys.system_id import SystemID
from scs_dev.sampler.gases_sampler import GasesSampler
from scs_dfe.board.dfe_conf import DFEConf
from scs_dfe.climate.sht_conf import SHTConf
from scs_host.bus.i2c import I2C
from scs_host.sync.schedule_runner import ScheduleRunner
from scs_host.sys.host import Host

ROOT = '/home/pi/SCS/'
root_dev = ROOT + 'scs_dev/src/scs_dev/'
os.chdir(root_dev)


### CherryPy server class
class GasesSamplerServer():


    @cherrypy.expose
    def i2c_open(self):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        cherrypy.response.headers['Server'] = 'SCSAlphasensePythonServer'

        I2C.open(Host.I2C_SENSORS)
        return json.dumps({'I2Copened': True}).encode('ascii')

    @cherrypy.expose
    def i2c_close(self):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        cherrypy.response.headers['Server'] = 'SCSAlphasensePythonServer'

        I2C.close()
        return json.dumps({'I2Cclosed': True}).encode('ascii')

    @cherrypy.expose
    def commence_sampler(self, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        cherrypy.response.headers['Server'] = 'SCSAlphasensePythonServer'
        samples = kwargs['samples']
        return self.initiate_sampler(samples=samples)

    def initiate_sampler(self, samples=1000000):
        '''Reads configs, initiates runner and sampler'''

        try:
            from scs_ndir.gas.ndir_conf import NDIRConf
        except ImportError:
            from scs_core.gas.ndir_conf import NDIRConf

        I2C.open(Host.I2C_SENSORS)

        sampler = None
        system_id = SystemID.load(Host)

        if system_id is None:
            return "gases_sampler: SystemID not available"

        # NDIR...
        ndir_conf = NDIRConf.load(Host)
        ndir_monitor = None if ndir_conf is None else ndir_conf.ndir_monitor(Host)

        # SHT...
        sht_conf = SHTConf.load(Host)
        sht = None if sht_conf is None else sht_conf.int_sht()

        # AFE...
        dfe_conf = DFEConf.load(Host)
        afe = None if dfe_conf is None else dfe_conf.afe(Host)

        # runner...
        _interval = 1
        runner = TimedRunner(_interval, int(samples))

        # sampler...
        sampler = GasesSampler(runner, system_id.message_tag(), ndir_monitor, sht, afe)
        sampler.start()

        #self.sampler = sampler
        self.samples = sampler.samples()
        return json.dumps({'commence_sampler': sampler.__str__()}).encode()


    @cherrypy.expose
    def sampling(self):
        '''The function to use in MATLAB frontend: request to exactly this path are sent from the client'''

        cherrypy.response.headers['Content-Type'] = 'application/json'
        cherrypy.response.headers['Server'] = 'SCSAlphasensePythonServer'

        try:
            _dummy_ = self.samples
        except AttributeError:
            try:
                self.initiate_sampler()
            except RuntimeError as e:
                I2C.open(Host.I2C_SENSORS)
                self.initiate_sampler()
        finally:
            asample = next(self.samples)
            return JSONify.dumps(asample).encode()

    @cherrypy.expose
    def start_process(self, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        if not self.process:
            tag = kwargs['tag']
            p = Popen(['python3', '/home/pi/scs-aryballe/gases-sampler-standalone.py', '-t', tag])
            self.process = p
            return json.dumps({'launched': True, 'pid': p.pid}).encode()
        else:
            return json.dumps({'error': 'Process already spawned with PID: {}'.format(self.process.pid),
                               'method': 'start_process'}).encode()

    @cherrypy.expose
    def stop_process(self, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        if self.process:
            pid = self.process.pid
            self.process.kill()
            self.process = None
            return json.dumps({'killed': True, 'pid': pid}).encode()
        else:
            return json.dumps({'error': 'No process spawned so far',
                               'method': 'stop_process'}).encode()

    @cherrypy.expose
    def get_process(self, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        is_running = True if self.process else False
        pid = self.process.pid if self.process else None
        return json.dumps({'is_running': is_running, 'pid': pid}).encode()



if __name__ == '__main__':
    cherrypy.config.update({'server.socket_host': '0.0.0.0',
                            'response.stream': True})
    listener = GasesSamplerServer()
    listener.process = None
    cherrypy.quickstart(listener)

