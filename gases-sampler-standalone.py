import pymongo
import requests
import json
import os
import time
import sys
import signal
import gpiozero

### Copy of imports from gases_sampler.py
from scs_host.bus.i2c import I2C
from scs_host.sys.host import Host
import sys
from scs_core.data.json import JSONify
from scs_core.data.localized_datetime import LocalizedDatetime
from scs_core.sync.timed_runner import TimedRunner
from scs_core.sys.system_id import SystemID
from scs_dev.sampler.gases_sampler import GasesSampler
from scs_dfe.board.dfe_conf import DFEConf
from scs_dfe.climate.sht_conf import SHTConf
from scs_host.bus.i2c import I2C
from scs_host.sync.schedule_runner import ScheduleRunner
from scs_host.sys.host import Host

ROOT = '/home/pi/SCS/'
root_dev = ROOT + 'scs_dev/src/scs_dev/'
os.chdir(root_dev)

mongodb_user = 'userAryballe'
mongodb_pass = 'kmsr890714mongodbaryballe'

class MongoDB():
    def __init__(self, remote=True):
        if remote:
            self.client = pymongo.MongoClient('mongodb://{}:{}@musifybot.com'.format(mongodb_user, mongodb_pass), 27017)
        else:
            self.client = pymongo.MongoClient('mongodb://localhost', 27017)
        self.db = self.client.scsdata
        self.collection = self.db.samples


class StandaloneSampler():
    def __init__(self):
        self.initiate_sampler()

    def initiate_sampler(self, samples=1000000):
            '''Reads configs, initiates runner and sampler'''

            try:
                from scs_ndir.gas.ndir_conf import NDIRConf
            except ImportError:
                from scs_core.gas.ndir_conf import NDIRConf

            sampler = None
            system_id = SystemID.load(Host)

            I2C.open(Host.I2C_SENSORS)

            if system_id is None:
                return "gases_sampler: SystemID not available"

            # NDIR...
            ndir_conf = NDIRConf.load(Host)
            ndir_monitor = None if ndir_conf is None else ndir_conf.ndir_monitor(Host)

            # SHT...
            sht_conf = SHTConf.load(Host)
            sht = None if sht_conf is None else sht_conf.int_sht()

            # AFE...
            dfe_conf = DFEConf.load(Host)
            afe = None if dfe_conf is None else dfe_conf.afe(Host)

            # runner...
            _interval = 1
            runner = TimedRunner(_interval, int(samples))

            # sampler...
            sampler = GasesSampler(runner, system_id.message_tag(), ndir_monitor, sht, afe)
            sampler.start()

            #self.sampler = sampler
            self.samples = sampler.samples()
            return json.dumps({'commence_sampler': sampler.__str__()}).encode()


    def sampling(self):
        '''The function to use in MATLAB frontend: request to exactly this path are sent from the client'''

        try:
            _dummy_ = self.samples
        except AttributeError:
            print('Handling the AttributeError exception..')
            try:
                self.initiate_sampler()
            except RuntimeError as e:
                print('Handling the RuntimeError exception..')
                I2C.open(Host.I2C_SENSORS)
                self.initiate_sampler()
        finally:
            asample = next(self.samples)
            return JSONify.dumps(asample)#.encode()

def main(tag):
    mysampler = StandaloneSampler()
    db = MongoDB()
    tag = tag if tag else 'noTag'

    while True:
        try:
            sample = mysampler.sampling()
        except Exception as e:
            print('{} : {}'.format(tag, e))
        document = json.loads(sample)
        document['tag'] = tag
        document['ts'] = time.time()
        result = db.collection.insert_one(document)
        print('{} : {} : {}'.format(tag, result.inserted_id, result.acknowledged))

    
if __name__ == '__main__':
    from getopt import getopt
    from sys import argv, exit

    tag, remote = None, True

    [opts, args] = getopt(argv[1:] ,"t:r:")
    for opt, arg in opts:
        if opt in ['-t']:
            tag = arg

    led = gpiozero.LED(21)
    led.on()

    def sigterm_handler(signal, frame):
        led.off()
        sys.exit(0)

    signal.signal(signal.SIGTERM, sigterm_handler)

    try:
        main(tag)
    except:
        led.off()
        sys.exit(0)