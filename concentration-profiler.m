clear 
url = 'http://192.168.1.145:8080/sampling';
%options = weboptions('ContentType','json');
%y = 0;
%x = 0 ;
t = 0;
startSpot = 0;
interv = 10000;
step = 1 ;
wholedata = {};
while ( t < interv )
    %b = sin(t)+5;
    b = webread(url);
    wholedata = [wholedata; {b}];
    save('wholedata_dump_.mat', 'wholedata');
    c = b.val.VOC.cnc;
    time = posixtime(datetime(b.rec, 'InputFormat','uuuu-MM-dd''T''HH:mm:ss.SSSXXXX','TimeZone','UTC'));
    if t == 0
        t0 = time;
        tt = 0;
        y = tt;
        x = c;
    else
        tt = time - t0;
        y = [ y, tt];
        x = [ x, c ];
    end
    plot(y, x, 'r', 'LineWidth', 2);
      if ((tt/step)-1000 < 0)
          startSpot = 2;
      else
          startSpot = (tt/step)-1000;
      end
      axis([ startSpot, (tt/step+100), 0.5*min(x(:,1:end)) , 2*max(x(:,1:end)) ]);
      grid
      t = t + step;
      drawnow;
      pause(0.01)
  end